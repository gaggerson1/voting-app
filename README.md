This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Installation

##### You’ll need to have Node 8.10.0 or later on your local development machine you can download node from [here](https://nodejs.org/es/).

1. Open a terminal
1. Go to the project root and execute this command

```bash
npm install

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
