/**
 * Libraries
 */
import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

/**
 * Styles
 */
import './VoteProgress.scss';

/**
 * Assets
 */
import icons from '../../assets/font-icon/voting/icons.svg';

const Progress = (props) => {
  const getTotal = () => props.votes.up + props.votes.down;

  const getPercentage = (value) => {
    let total = getTotal();
    let percentage = Math.round((value * 100) / total);
    return percentage;
  }

  const upPercentage = getPercentage(props.votes.up);
  const downPercentage = getPercentage(props.votes.down);

  return (
    <div
      className={classNames("voting-vote-progress")}
    >
      {
        upPercentage > 0 && (
          <div
            className={classNames("voting-vote-progress__up-wrapper")}
            style={{ width: `${upPercentage}%` }}
          >
            <svg
              className={classNames("voting-vote-progress__icon", "voting-vote-progress__up-wrapper-icon")}
            >
              <use
                xlinkHref={`${icons}#voting-icon-thumbs-up`}
              >
              </use>
            </svg>
            <span
              className={classNames("voting-vote-progress__percentage")}
            >
              {`${upPercentage}%`}
            </span>
          </div>
        )
      }
      {
        downPercentage > 0 && (
          <div
            className={classNames("voting-vote-progress__down-wrapper")}
            style={{ width: `${downPercentage}%` }}
          >
            <span
              className={classNames("voting-vote-progress__percentage")}
            >
              {`${downPercentage}%`}
            </span>
            <svg
              className={classNames("voting-vote-progress__icon", "voting-vote-progress__down-wrapper-icon")}
            >
              <use
                xlinkHref={`${icons}#voting-icon-thumbs-down`}
              >
              </use>
            </svg>
          </div>
        )
      }
    </div>
  );
}

Progress.propTypes = {
  votes: PropTypes.shape({
    up: PropTypes.number,
    down: PropTypes.down,
  })
}

export default Progress;
