/**
 * Libraries
 */
import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import classNames from 'classnames';

/**
 * Dependencies
 */
import routes from '../../constants/routes';

/**
 * Styles
 */
import './NavBar.scss';

/**
 * Assets
 */
import icons from '../../assets/font-icon/voting/icons.svg';

class NavBar extends Component {
  state = {
    opened: false,
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }

  handleToggleMenu = () => {
    const olValue = this.state.opened;
    this.updateMenuState(!olValue);
  }

  handleResize = (e) => {
    const width = window.outerWidth;
    if (width > 767) {
      this.updateMenuState(false);
    }
  }

  updateMenuState = (value) => {
    this.setState({
      opened: !!value,
    })
  }

  render() {
    return (
      <div
        className={classNames("voting-nav-bar")}
      >
        <button
          className={classNames("voting-button", "voting-button--icon-button", "voting-nav-bar__button", this.state.opened && "voting-nav-bar__button--opened")}
          onClick={this.handleToggleMenu}
        >
          <div></div>
          <div></div>
          <div></div>
        </button>
        <nav
          className={classNames("voting-nav", "voting-nav--mobile", "voting-home__main-section-header-nav", "voting-nav-bar__nav", this.state.opened && "voting-nav-bar__nav--opened")}
        >
          <NavLink
            to={routes.trials}
          >
            Past Trials
          </NavLink>
          <NavLink
            to={routes.howItWorks}
          >
            How It Works
          </NavLink>
          <NavLink
            to={routes.signin}
          >
            Log In / Sign Up
          </NavLink>
          <NavLink
            className={classNames("voting-button", "voting-button--icon-button")}
            to={routes.search}
          >
            <svg>
              <use
                xlinkHref={`${icons}#voting-icon-search`}
              >
              </use>
            </svg>
          </NavLink>
        </nav>
      </div>
    );
  }
}

export default NavBar;
