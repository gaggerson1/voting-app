/**
 * Libraries
 */
import React, { Component, Fragment } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

/**
 * Components
 */
import VoteProgress from '../VoteProgress';

/**
 * Styles
 */
import './Card.scss';

/**
 * Assets
 */
import icons from '../../assets/font-icon/voting/icons.svg';

class Card extends Component {

  state = {
    voting: true,
    voteValue: true,
  }

  static propTypes = {
    data: PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      description: PropTypes.string,
      picture: PropTypes.string,
      area: PropTypes.string,
      time: PropTypes.string,
      votes: PropTypes.shape({
        up: PropTypes.number,
        down: PropTypes.number,
      }),
    }),
    onVote: PropTypes.func,
  }

  static defaultProps = {
    onVote: () => { },
  }

  handleSelectVote = (value) => {
    return e => {
      this.updateVoteValueState(value);
    }
  }

  handleVote = () => {
    this.props.onVote(this.state.voteValue, this.props.data.id);
    this.updateVotingState(false);
    this.updateVoteValueState(true);
  }

  handleVoteAgain = () => {
    this.updateVotingState(true);
  }

  updateVotingState = (value) => {
    this.setState({
      voting: !!value,
    })
  }

  updateVoteValueState = (value) => {
    this.setState({
      voteValue: !!value,
    })
  }

  render() {
    const classes = classNames('voting-card', this.props.className);

    return (
      <div
        className={classes}
      >
        <img
          className={classNames("voting-card__bgr-image")}
          src={this.props.data.picture}
          alt={this.props.data.name}
        />
        <div
          className={classNames("voting-card__wrapper")}
        >
          <div
            className={classNames("voting-card__content")}
          >
            {
              this.renderTag()
            }
            <h2
              className={classNames("voting-card__name")}
            >
              {this.props.data.name}
            </h2>
            <p
              className={classNames("voting-card__time")}
            >
              {`${this.props.data.time} in ${this.props.data.area}`}
            </p>
            {
              {
                true:
                  (
                    <Fragment>
                      <p
                        className={classNames("voting-card__description")}
                      >
                        {this.props.data.description}
                      </p>
                      <div
                        className={classNames("voting-card__actions")}
                      >
                        <button
                          className={classNames("voting-button", "voting-button--icon-button", "voting-button--bg-jave", "voting-card__action-button", !!this.state.voteValue && ("voting-button--bordered voting-button--bordered--white"))}
                          onClick={this.handleSelectVote(true)}
                        >
                          <svg>
                            <use
                              xlinkHref={`${icons}#voting-icon-thumbs-up`}
                            >
                            </use>
                          </svg>
                        </button>
                        <button
                          className={classNames("voting-button", "voting-button--icon-button", "voting-button--bg-my-sin", "voting-card__action-button", !this.state.voteValue && ("voting-button--bordered voting-button--bordered--white"))}
                          onClick={this.handleSelectVote(false)}
                        >
                          <svg>
                            <use
                              xlinkHref={`${icons}#voting-icon-thumbs-down`}
                            >
                            </use>
                          </svg>
                        </button>
                        <button
                          className={classNames("voting-button", "voting-button--outline", "voting-button--outline--white", "voting-card__action-button")}
                          onClick={this.handleVote}
                        >
                          Vote Now
                      </button>
                      </div>
                    </Fragment>
                  ),
                false:
                  (
                    <Fragment>
                      <p
                        className={classNames("voting-card__description", "voting-card__description--thanks")}
                      >
                        Thank you for voting!
                      </p>
                      <button
                        className={classNames("voting-button", "voting-button--outline", "voting-button--outline--white", "voting-card__action-button")}
                        onClick={this.handleVoteAgain}
                      >
                        Vote again
                      </button>
                    </Fragment>
                  )
              }[this.state.voting]
            }
          </div>
          <VoteProgress votes={this.props.data.votes} />
        </div>
      </div>
    );
  }

  renderTag = () => {
    return (
      (this.props.data.votes.up > this.props.data.votes.down)
        ?
        (
          <span
            className={classNames("voting-button", "voting-button--icon-button", "voting-button--bg-jave", "voting-button--without-events", "voting-card__content-button")}
          >
            <svg>
              <use
                xlinkHref={`${icons}#voting-icon-thumbs-up`}
              >
              </use>
            </svg>
          </span>
        )
        : (this.props.data.votes.up < this.props.data.votes.down)
          ?
          (
            <span
              className={classNames("voting-button", "voting-button--icon-button", "voting-button--bg-my-sin", "voting-button--without-events", "voting-card__content-button")}
            >
              <svg>
                <use
                  xlinkHref={`${icons}#voting-icon-thumbs-down`}
                >
                </use>
              </svg>
            </span>
          )
          : null
    )
  }
}

export default Card;
