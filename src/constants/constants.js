import kanyeImage from '../assets/images/people/kanye.png';
import markImage from '../assets/images/people/mark.png';
import cristinaImage from '../assets/images/people/cristina.png';
import malalaImage from '../assets/images/people/malala.png';

const LOCALSTORAGE = {
  votingData: 'voting_data',
}

const PEOPLE = [{
    id: 1,
    name: 'Kanye West',
    description: 'Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.',
    picture: kanyeImage,
    area: 'Entertainment',
    time: '1 month ago',
    votes: {
      up: 64,
      down: 36,
    }
  },
  {
    id: 2,
    name: 'Mark Zuckerberg',
    description: 'Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.',
    picture: markImage,
    area: 'Bussines',
    time: '1 month ago',
    votes: {
      up: 36,
      down: 64,
    }
  },
  {
    id: 3,
    name: 'Cristina Fernández de Kirchner',
    description: 'Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.',
    picture: cristinaImage,
    area: 'Politics',
    time: '1 month ago',
    votes: {
      up: 36,
      down: 64,
    }
  },
  {
    id: 4,
    name: 'Malala Yousafzai',
    description: 'Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.',
    picture: malalaImage,
    area: 'Entertainment',
    time: '1 month ago',
    votes: {
      up: 64,
      down: 36,
    }
  }
]

export {
  LOCALSTORAGE,
  PEOPLE,
}