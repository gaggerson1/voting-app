/**
 * Libraries
 */
import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import classNames from 'classnames';

/**
 * Dependencies
 */
import routes from '../../constants/routes';
import * as localstorage from '../../services/localstorage';

/**
 * Components
 */
import Card from '../../components/Card';
import CardFeatured from '../../components/Card/Featured';
import NavBar from '../../components/NavBar';

/**
 * Constants
 */
import { PEOPLE, LOCALSTORAGE } from '../../constants/constants';

/**
 * Styles
 */
import './Home.scss';

/**
 * Assets
 */
import icons from '../../assets/font-icon/voting/icons.svg';
import papaImage from '../../assets/images/header.png';

class Home extends Component {
  state = {
    featured: {
      name: 'Pope Francis?',
      description: 'He’s talking tough on clergy sexual abuse, but is he just another papal pervert protector? (thumbs down) or a true pedophile punishing pontiff? (thumbs up)',
      picture: papaImage,
    },
    people: [],
  }

  componentDidMount() {
    this.getPeople();
  }

  handleVote = (value, id) => {
    const people = [...this.state.people];
    const personIndex = people.findIndex((p) => (p.id === id));
    const oldPerson = people[personIndex];
    const newPerson = {
      ...oldPerson,
      votes: {
        up: (!!value) ? oldPerson.votes.up + 1 : oldPerson.votes.up,
        down: (!value) ? oldPerson.votes.down + 1 : oldPerson.votes.down,
      }
    }

    this.setState({
      people: [
        ...people.slice(0, personIndex),
        newPerson,
        ...people.slice(personIndex + 1, people.length),
      ],
    }, () => {
      this.savePeople(this.state.people);
    })

  }

  getPeople = () => {
    const people = localstorage.getObject(LOCALSTORAGE.votingData) || PEOPLE;
    this.setState({
      people: [
        ...people,
      ]
    })
  }

  savePeople = (people) => {
    localstorage.setObject(LOCALSTORAGE.votingData, people);
  }

  render() {
    return (
      <div
        className={classNames("voting-home")}
      >
        <section
          className={classNames("voting-home__main-section")}
        >
          <div
            className={classNames("voting-home__main-section-header-wrapper")}
          >
            <header
              className={classNames("voting-home__main-section-header")}
            >
              <h1
                className={classNames("voting-home__main-section-header-title")}
              >
                Rule of Thumb.
              </h1>
              <NavBar />
            </header>
          </div>
          <div
            className={classNames("voting-home__main-section-content")}
          >
            <CardFeatured
              data={this.state.featured}
            />
          </div>
          <div
            className={classNames("voting-home__main-section-footer")}
          >
            <div
              className={classNames("voting-closing-bar")}
            >
              <div
                className={classNames("voting-closing-bar__message-wrapper")}
              >
                <span
                  className={classNames("voting-closing-bar__message")}
                >
                  CLOSING IN
                </span>
              </div>
              <div
                className={classNames("voting-closing-bar__days-wrapper")}
              >
                <span
                  className={classNames("voting-closing-bar__number-days")}
                >
                  22
                </span>
                <span
                  className={classNames("voting-closing-bar__text-days")}
                >
                  days
                </span>
              </div>
            </div>
          </div>
        </section>
        <div
          className={classNames("voting-home__wrapper")}
        >
          <section
            className={classNames("voting-home__body-section")}
          >
            <div
              className={classNames("voting-alert", "voting-home__body-section-alert")}
            >
              <div
                className={classNames("voting-alert__left-info")}
              >
                <span
                  className={classNames("voting-alert__subheading")}
                >
                  Speak out. Be heard.
                </span>
                <span
                  className={classNames("voting-alert__heading")}
                >
                  Be counted
                </span>
              </div>
              <span
                className={classNames("voting-alert__text")}
              >
                Rule of Thumb is a crowd sourced court of public opinion where anyone and everyone can speak out and speak freely. It’s easy: You share your opinion, we analyze and put the data in a public report.
              </span>
              <button
                className={classNames("voting-button", "voting-button--icon-button", "voting-alert__button")}
              >
                <svg>
                  <use
                    xlinkHref={`${icons}#voting-icon-cross-out`}
                  >
                  </use>
                </svg>
              </button>
            </div>
            <h1
              className={classNames("voting-home__body-section-title")}
            >
              Votes
            </h1>
            <div
              className={classNames("voting-home__body-section-cards-wrapper")}
            >
              {
                this.state.people.map((p, index) => (
                  <Card
                    key={index}
                    className={classNames("voting-home__body-section-card ")}
                    data={p}
                    onVote={this.handleVote}
                  />
                ))
              }
            </div>
            <div
              className={classNames("voting-home__body-section-submit-wrapper")}
            >
              <span
                className={classNames("voting-home__body-section-submit-wrapper-text")}
              >
                Is there anyone else you would want us to add?
              </span>
              <button
                className={classNames("voting-button", "voting-button--outline", "voting-button--outline--mine-shaft", "voting-home__body-section-submit-wrapper-button")}
              >
                Submit a Name
              </button>
            </div>
          </section>
          <footer
            className={classNames("voting-home__footer-section")}
          >
            <nav
              className={classNames("voting-nav", "voting-nav--alternative", "voting-home__footer-section-nav")}
            >
              <NavLink
                className={classNames("voting-home__footer-section-nav__link")}
                to={routes.trials}
              >
                Past Trials
              </NavLink>
              <NavLink
                className={classNames("voting-home__footer-section-nav__link")}
                to={routes.howItWorks}
              >
                How It Works
              </NavLink>
              <NavLink
                className={classNames("voting-home__footer-section-nav__link")}
                to={routes.signin}
              >
                Log In / Sign Up
              </NavLink>
            </nav>
            <div
              className={classNames("voting-home__footer-follow")}
            >
              <span
                className={classNames("voting-home__footer-follow-text")}
              >
                Follow Us
              </span>
              <ul
                className={classNames("voting-home__footer-follow-list")}
              >
                <li
                  className={classNames("voting-home__footer-follow-action")}
                >
                  <a
                    className={classNames("voting-home__footer-follow-action-link")}
                    href="https://www.facebook.com/"
                  >
                    <svg>
                      <use
                        xlinkHref={`${icons}#voting-icon-facebook`}
                      >
                      </use>
                    </svg>
                  </a>
                </li>
                <li
                  className={classNames("voting-home__footer-follow-action")}
                >
                  <a
                    className={classNames("voting-home__footer-follow-action-link")}
                    href="https://twitter.com/?lang=es"
                  >
                    <svg>
                      <use
                        xlinkHref={`${icons}#voting-icon-twitter`}
                      >
                      </use>
                    </svg>
                  </a>
                </li>
              </ul>
            </div>
          </footer>
        </div>
      </div>
    );
  }
}

export default Home;
