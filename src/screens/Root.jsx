/**
 * Libraries
 */
import React from 'react';

/**
 * Dependencies
 */
import 'typeface-lato';

/**
 * Screens
 */
import Routes from './Routes';

/**
 * Styles
 */
import '../styles/main.scss';

const Root = () => {
  return (
    <React.Fragment>
      <Routes />
    </React.Fragment>
  );
}

export default Root;
